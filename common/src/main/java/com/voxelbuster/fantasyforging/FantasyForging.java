/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.voxelbuster.fantasyforging;

import com.google.common.base.Suppliers;
import com.voxelbuster.fantasyforging.block.FFBlocks;
import com.voxelbuster.fantasyforging.item.FFItems;
import dev.architectury.registry.CreativeTabRegistry;
import dev.architectury.registry.registries.Registrar;
import dev.architectury.registry.registries.RegistrarManager;
import lombok.Getter;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Block;

import java.util.function.Supplier;

public class FantasyForging {
    public static final String MOD_ID = "fantasy_forging";

    public static final Supplier<RegistrarManager> MANAGER = Suppliers.memoize(() -> RegistrarManager.get(MOD_ID));

    @Getter
    private static Registrar<Item> items;

    @Getter
    private static Registrar<Block> blocks;

    @Getter
    private static Registrar<CreativeModeTab> creativeTabs;

    @Getter
    private static CreativeModeTab materialsTab = CreativeTabRegistry.create(Component.literal("Fantasy Forging Materials"), () -> new ItemStack(Items.RAW_IRON));

    @Getter
    private static CreativeModeTab blocksTab = CreativeTabRegistry.create(Component.literal("Fantasy Forging Blocks"), () -> new ItemStack(Items.GOLD_ORE));


    public static void init() {
        items = MANAGER.get().get(Registries.ITEM);
        blocks = MANAGER.get().get(Registries.BLOCK);
        creativeTabs = MANAGER.get().get(Registries.CREATIVE_MODE_TAB);

        creativeTabs.register(createRL("materials_tab"), () -> materialsTab);
        creativeTabs.register(createRL("blocks_tab"), () -> blocksTab);

        FFItems.register();
        FFBlocks.register();
    }

    public static ResourceLocation createRL(String location) {
        return new ResourceLocation(MOD_ID, location);
    }
}
