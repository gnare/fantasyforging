/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.voxelbuster.fantasyforging.tags;

import com.google.common.collect.Iterators;
import lombok.Getter;
import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;

@Getter
public class FFRarity {
    private static final Iterator<ChatFormatting> rainbowFormat = Iterators.cycle(
        ChatFormatting.LIGHT_PURPLE,
        ChatFormatting.DARK_PURPLE,
        ChatFormatting.BLUE,
        ChatFormatting.AQUA,
        ChatFormatting.GREEN,
        ChatFormatting.YELLOW,
        ChatFormatting.GOLD,
        ChatFormatting.RED
    );

    public static final FFRarity COMMON = new FFRarity(ChatFormatting.WHITE);
    public static final FFRarity UNCOMMON = new FFRarity(ChatFormatting.GREEN);
    public static final FFRarity MAGIC = new FFRarity(ChatFormatting.AQUA);
    public static final FFRarity RARE = new FFRarity(ChatFormatting.YELLOW);
    public static final FFRarity EPIC = new FFRarity(ChatFormatting.LIGHT_PURPLE);
    public static final FFRarity LEGENDARY = new FFRarity(ChatFormatting.GOLD);
    public static final FFRarity MYTHICAL = new FFRarity(ChatFormatting.DARK_RED);
    public static final FFRarity RADIANT = new FFRarity(null);

//    private final ArrayList<FFRarity> rarityOrder = new ArrayList<>(List.of(
//        COMMON,
//        UNCOMMON,
//        MAGIC,
//        RARE,
//        EPIC,
//        LEGENDARY,
//        MYTHICAL,
//        RADIANT
//    ));

    @Nullable
    private final ChatFormatting color;

    public Component getFormattedComponent(String text) {
        MutableComponent component = Component.literal("");
        if (this == RADIANT) {
            Arrays.stream(text.split(""))
                .forEach(c -> component.append(Component.literal(c).withStyle(rainbowFormat.next())));
        } else {
            component.append(Component.literal(text).withStyle(Objects.requireNonNullElse(color, ChatFormatting.WHITE)));
        }
        return component;
    }

    private FFRarity(@Nullable ChatFormatting chatChatFormatting) {
        this.color = chatChatFormatting;
    }

}
