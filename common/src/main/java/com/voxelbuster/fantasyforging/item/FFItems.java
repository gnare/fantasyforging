/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.voxelbuster.fantasyforging.item;

import com.voxelbuster.fantasyforging.FantasyForging;
import com.voxelbuster.fantasyforging.tags.FFRarity;
import com.voxelbuster.mods.datagen.annotations.DataGenNamespace;
import com.voxelbuster.mods.datagen.annotations.ItemDataGen;
import com.voxelbuster.mods.datagen.annotations.TagWith;
import dev.architectury.registry.registries.Registrar;
import dev.architectury.registry.registries.RegistrySupplier;
import lombok.Getter;
import net.minecraft.world.item.Item;

import static com.voxelbuster.fantasyforging.FantasyForging.MOD_ID;
import static com.voxelbuster.fantasyforging.FantasyForging.createRL;

@Getter
@DataGenNamespace(MOD_ID)
public final class FFItems {


    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/lightless"}, tagNamespace = "")// Ingots
    @ItemDataGen(en_us = "Lightless Ingot")
    private static RegistrySupplier<FFItemBase> lightless_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/tin"}, tagNamespace = "")
    @ItemDataGen(en_us = "Tin Ingot")
    private static RegistrySupplier<FFItemBase> tin_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/lead"}, tagNamespace = "")
    @ItemDataGen(en_us = "Lead Ingot")
    private static RegistrySupplier<FFItemBase> lead_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/aluminum"}, tagNamespace = "")
    @ItemDataGen(en_us = "Aluminum Ingot")
    private static RegistrySupplier<FFItemBase> aluminum_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/silver"}, tagNamespace = "")
    @ItemDataGen(en_us = "Silver Ingot")
    private static RegistrySupplier<FFItemBase> silver_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/tungsten"}, tagNamespace = "")
    @ItemDataGen(en_us = "Tungsten Ingot")
    private static RegistrySupplier<FFItemBase> tungsten_ingot;


    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/titanium"}, tagNamespace = "")
    @ItemDataGen(en_us = "Titanium Ingot")
    private static RegistrySupplier<FFItemBase> titanium_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/platinum"}, tagNamespace = "")
    @ItemDataGen(en_us = "Platinum Ingot")
    private static RegistrySupplier<FFItemBase> platinum_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/cobalt"}, tagNamespace = "")
    @ItemDataGen(en_us = "Cobalt Ingot")
    private static RegistrySupplier<FFItemBase> cobalt_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/osmium"}, tagNamespace = "")
    @ItemDataGen(en_us = "Osmium Ingot")
    private static RegistrySupplier<FFItemBase> osmium_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/bronze"}, tagNamespace = "")
    @ItemDataGen(en_us = "Bronze Ingot")
    private static RegistrySupplier<FFItemBase> bronze_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/brass"}, tagNamespace = "")
    @ItemDataGen(en_us = "Brass Ingot")
    private static RegistrySupplier<FFItemBase> brass_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/steel"}, tagNamespace = "")
    @ItemDataGen(en_us = "Steel Ingot")
    private static RegistrySupplier<FFItemBase> steel_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/electrum"}, tagNamespace = "")
    @ItemDataGen(en_us = "Electrum Ingot")
    private static RegistrySupplier<FFItemBase> electrum_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/white_gold"}, tagNamespace = "")
    @ItemDataGen(en_us = "White Gold Ingot")
    private static RegistrySupplier<FFItemBase> white_gold_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/cryosilver"}, tagNamespace = "")
    @ItemDataGen(en_us = "Cryosilver Ingot")
    private static RegistrySupplier<FFItemBase> cryosilver_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/prismarium"}, tagNamespace = "")
    @ItemDataGen(en_us = "Prismarium Ingot")
    private static RegistrySupplier<FFItemBase> prismarium_ingot;


    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/enderium"}, tagNamespace = "")
    @ItemDataGen(en_us = "Enderium Ingot")
    private static RegistrySupplier<FFItemBase> enderium_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/durasteel"}, tagNamespace = "")
    @ItemDataGen(en_us = "Durasteel Ingot")
    private static RegistrySupplier<FFItemBase> durasteel_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/tungsten_carbide"}, tagNamespace = "")
    @ItemDataGen(en_us = "Tungsten Carbide Ingot")
    private static RegistrySupplier<FFItemBase> tungsten_carbide_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/mithril"}, tagNamespace = "")
    @ItemDataGen(en_us = "Mithril Ingot")
    private static RegistrySupplier<FFItemBase> mithril_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/orichalcum"}, tagNamespace = "")
    @ItemDataGen(en_us = "Orichalcum Ingot")
    private static RegistrySupplier<FFItemBase> orichalcum_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/adamantite"}, tagNamespace = "")
    @ItemDataGen(en_us = "Adamantite Ingot")
    private static RegistrySupplier<FFItemBase> adamantite_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/khlorocyte"}, tagNamespace = "")
    @ItemDataGen(en_us = "Khlorocyte Ingot")
    private static RegistrySupplier<FFItemBase> khlorocyte_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/mycelite"}, tagNamespace = "")
    @ItemDataGen(en_us = "Mycelite Ingot")
    private static RegistrySupplier<FFItemBase> mycelite_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/stellarium"}, tagNamespace = "")
    @ItemDataGen(en_us = "Stellarium Ingot")
    private static RegistrySupplier<FFItemBase> stellarium_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/lunarium"}, tagNamespace = "")
    @ItemDataGen(en_us = "Lunarium Ingot")
    private static RegistrySupplier<FFItemBase> lunarium_ingot;


    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/fae_steel"}, tagNamespace = "")
    @ItemDataGen(en_us = "Fae Steel Ingot")
    private static RegistrySupplier<FFItemBase> fae_steel_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/sunsteel"}, tagNamespace = "")
    @ItemDataGen(en_us = "Sunsteel Ingot")
    private static RegistrySupplier<FFItemBase> sunsteel_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/moonsilver"}, tagNamespace = "")
    @ItemDataGen(en_us = "Moonsilver Ingot")
    private static RegistrySupplier<FFItemBase> moonsilver_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/aetherium"}, tagNamespace = "")
    @ItemDataGen(en_us = "Aetherium Ingot")
    private static RegistrySupplier<FFItemBase> aetherium_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/abyssal"}, tagNamespace = "")
    @ItemDataGen(en_us = "Abyssal Ingot")
    private static RegistrySupplier<FFItemBase> abyssal_ingot;


    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/gaia_alloy"}, tagNamespace = "")
    @ItemDataGen(en_us = "Gaia Alloy Ingot")
    private static RegistrySupplier<FFItemBase> gaia_alloy_ingot;

    @TagWith(value = {"c:items/ingots", "forge:ingots", "forge:ingots/celestial_alloy"}, tagNamespace = "")
    @ItemDataGen(en_us = "Celestial Alloy Ingot")
    private static RegistrySupplier<FFItemBase> celestial_alloy_ingot;

    // Raw Metals
    @TagWith(value = {"c:items/raw_ores", "forge:raw_materials", "forge:raw_materials/lightless"}, tagNamespace = "")
    @ItemDataGen(en_us = "Raw Lightless Metal")
    private static RegistrySupplier<FFItemBase> raw_lightless;

    @TagWith(value = {"c:items/raw_ores", "forge:raw_materials", "forge:raw_materials/tin"}, tagNamespace = "")
    @ItemDataGen(en_us = "Raw Tin")
    private static RegistrySupplier<FFItemBase> raw_tin;

    @TagWith(value = {"c:items/raw_ores", "forge:raw_materials", "forge:raw_materials/lead"}, tagNamespace = "")
    @ItemDataGen(en_us = "Raw Lead")
    private static RegistrySupplier<FFItemBase> raw_lead;

    @TagWith(value = {"c:items/raw_ores", "forge:raw_materials", "forge:raw_materials/aluminum"}, tagNamespace = "")
    @ItemDataGen(en_us = "Raw Aluminum")
    private static RegistrySupplier<FFItemBase> raw_aluminum;

    @TagWith(value = {"c:items/raw_ores", "forge:raw_materials", "forge:raw_materials/silver"}, tagNamespace = "")
    @ItemDataGen(en_us = "Raw Silver")
    private static RegistrySupplier<FFItemBase> raw_silver;

    @TagWith(value = {"c:items/raw_ores", "forge:raw_materials", "forge:raw_materials/tungsten"}, tagNamespace = "")
    @ItemDataGen(en_us = "Raw Tungsten")
    private static RegistrySupplier<FFItemBase> raw_tungsten;

    @TagWith(value = {"c:items/raw_ores", "forge:raw_materials", "forge:raw_materials/titanium"}, tagNamespace = "")
    @ItemDataGen(en_us = "Raw Titanium")
    private static RegistrySupplier<FFItemBase> raw_titanium;

    @TagWith(value = {"c:items/raw_ores", "forge:raw_materials", "forge:raw_materials/platinum"}, tagNamespace = "")
    @ItemDataGen(en_us = "Raw Platinum")
    private static RegistrySupplier<FFItemBase> raw_platinum;

    @TagWith(value = {"c:items/raw_ores", "forge:raw_materials", "forge:raw_materials/cobalt"}, tagNamespace = "")
    @ItemDataGen(en_us = "Raw Cobalt")
    private static RegistrySupplier<FFItemBase> raw_cobalt;

    @TagWith(value = {"c:items/raw_ores", "forge:raw_materials", "forge:raw_materials/osmium"}, tagNamespace = "")
    @ItemDataGen(en_us = "Raw Osmium")
    private static RegistrySupplier<FFItemBase> raw_osmium;

    @TagWith(value = {"c:items/raw_ores", "forge:raw_materials", "forge:raw_materials/cryosilver"}, tagNamespace = "")
    @ItemDataGen(en_us = "Raw Cryosilver")
    private static RegistrySupplier<FFItemBase> raw_cryosilver;

    @TagWith(value = {"c:items/raw_ores", "forge:raw_materials", "forge:raw_materials/prismarium"}, tagNamespace = "")
    @ItemDataGen(en_us = "Raw Prismarium")
    private static RegistrySupplier<FFItemBase> raw_prismarium;

    @TagWith(value = {"c:items/raw_ores", "forge:raw_materials", "forge:raw_materials/mithril"}, tagNamespace = "")
    @ItemDataGen(en_us = "Raw Mithril")
    private static RegistrySupplier<FFItemBase> raw_mithril;

    @TagWith(value = {"c:items/raw_ores", "forge:raw_materials", "forge:raw_materials/orichalcum"}, tagNamespace = "")
    @ItemDataGen(en_us = "Raw Oricalcum")
    private static RegistrySupplier<FFItemBase> raw_orichalcum;

    @TagWith(value = {"c:items/raw_ores", "forge:raw_materials", "forge:raw_materials/adamantite"}, tagNamespace = "")
    @ItemDataGen(en_us = "Raw Adamantite")
    private static RegistrySupplier<FFItemBase> raw_adamantite;

    @TagWith(value = {"c:items/raw_ores", "forge:raw_materials", "forge:raw_materials/khlorocyte"}, tagNamespace = "")
    @ItemDataGen(en_us = "Raw Khlorocyte")
    private static RegistrySupplier<FFItemBase> raw_khlorocyte;

    @TagWith(value = {"c:items/raw_ores", "forge:raw_materials", "forge:raw_materials/mycelite"}, tagNamespace = "")
    @ItemDataGen(en_us = "Raw Mycelite")
    private static RegistrySupplier<FFItemBase> raw_mycelite;

    @TagWith(value = {"c:items/raw_ores", "forge:raw_materials", "forge:raw_materials/stellarium"}, tagNamespace = "")
    @ItemDataGen(en_us = "Raw Stellarium")
    private static RegistrySupplier<FFItemBase> raw_stellarium;

    @TagWith(value = {"c:items/raw_ores", "forge:raw_materials", "forge:raw_materials/lunarium"}, tagNamespace = "")
    @ItemDataGen(en_us = "Raw Lunarium")
    private static RegistrySupplier<FFItemBase> raw_lunarium;

    public static void register() {
        Registrar<Item> items = FantasyForging.getItems();

        // Ingots
        lightless_ingot = items.register(createRL("lightless_ingot"), new FFItemBase(FFRarity.COMMON, "The magic in this metal has faded, but it might still be useful...").supplier());

        tin_ingot = items.register(createRL("tin_ingot"), new FFItemBase(FFRarity.COMMON, "It's tin. Nothing special.").supplier());
        lead_ingot = items.register(createRL("lead_ingot"), new FFItemBase(FFRarity.COMMON, "We don't recommend eating it.").supplier());
        aluminum_ingot = items.register(createRL("aluminum_ingot"), new FFItemBase(FFRarity.COMMON, "Pretty strong, and pretty light.").supplier());
        silver_ingot = items.register(createRL("silver_ingot"), new FFItemBase(FFRarity.COMMON, "Soft, shiny metal, great for alloys and jewelry.").supplier());
        tungsten_ingot = items.register(createRL("tungsten_ingot"), new FFItemBase(FFRarity.COMMON, "A very resilient metal; had to get the smeltery hotter than the Nether for this one.").supplier());

        titanium_ingot = items.register(createRL("titanium_ingot"), new FFItemBase(FFRarity.UNCOMMON, "Aptly named after divine titans.").supplier());
        platinum_ingot = items.register(createRL("platinum_ingot"), new FFItemBase(FFRarity.UNCOMMON, "Quite possibly the monarch of shiny metals.").supplier());
        cobalt_ingot = items.register(createRL("cobalt_ingot"), new FFItemBase(FFRarity.UNCOMMON, "Useful in alloys. It's also magnetic.").supplier());
        osmium_ingot = items.register(createRL("osmium_ingot"), new FFItemBase(FFRarity.UNCOMMON, "A dense metal descended from the skies above.").supplier());

        bronze_ingot = items.register(createRL("bronze_ingot"), new FFItemBase(FFRarity.UNCOMMON, "Baby's first alloy!").supplier());
        brass_ingot = items.register(createRL("brass_ingot"), new FFItemBase(FFRarity.UNCOMMON, "First discovered by the Knights of Slime.").supplier());
        steel_ingot = items.register(createRL("steel_ingot"), new FFItemBase(FFRarity.UNCOMMON, "Time to get your forge hammer, it's steel!").supplier());
        electrum_ingot = items.register(createRL("electrum_ingot"), new FFItemBase(FFRarity.UNCOMMON, "Quite a bit stronger than gold, but just as pretty.").supplier());
        white_gold_ingot = items.register(createRL("white_gold_ingot"), new FFItemBase(FFRarity.UNCOMMON, "For people who are too cool for regular gold rings.").supplier());
        cryosilver_ingot = items.register(createRL("cryosilver_ingot"), new FFItemBase(FFRarity.UNCOMMON, "A hard, but brittle metal that has to be kept cold at all times, lest it melt.").supplier());
        prismarium_ingot = items.register(createRL("prismarium_ingot"), new FFItemBase(FFRarity.UNCOMMON, "An ancient material used by the Guardians.").supplier());

        enderium_ingot = items.register(createRL("enderium_ingot"), new FFItemBase(FFRarity.MAGIC, "The energy in this material... it almost seems like it wants to get out.", false, true).supplier());
        durasteel_ingot = items.register(createRL("durasteel_ingot"), new FFItemBase(FFRarity.MAGIC, "Ultra-hard steel. You might be able to forge some really good tools out of this.").supplier());
        tungsten_carbide_ingot = items.register(createRL("tungsten_carbide_ingot"), new FFItemBase(FFRarity.MAGIC, "This metal can't be scratched by anything softer than diamond, and it took nothing short of an inferno to make it at all.", false, true).supplier());
        mithril_ingot = items.register(createRL("mithril_ingot"), new FFItemBase(FFRarity.MAGIC, "Somehow this metal is lighter than aluminum, but seems stronger than steel.").supplier());
        orichalcum_ingot = items.register(createRL("orichalcum_ingot"), new FFItemBase(FFRarity.MAGIC, "Mean and green.").supplier());
        adamantite_ingot = items.register(createRL("adamantite_ingot"), new FFItemBase(FFRarity.MAGIC, "Some believe that armor forged from adamantite is unbreakable.", false, true).supplier());
        khlorocyte_ingot = items.register(createRL("khlorocyte_ingot"), new FFItemBase(FFRarity.MAGIC, "A strange, almost plant-like metal that heals any scratches made on it over time.").supplier());
        mycelite_ingot = items.register(createRL("mycelite_ingot"), new FFItemBase(FFRarity.MAGIC, "A peculiar mineral that seems to have a fungal origin.").supplier());
        stellarium_ingot = items.register(createRL("stellarium_ingot"), new FFItemBase(FFRarity.MAGIC, "Warm to the touch. It also seems to be pulsating with a magical aura... it might not last long though.", true, true).supplier());
        lunarium_ingot = items.register(createRL("lunarium_ingot"), new FFItemBase(FFRarity.MAGIC, "Cool to the touch. It also seems to be pulsating with a magical aura... it might not last long though.", true, true).supplier());

        fae_steel_ingot = items.register(createRL("fae_steel_ingot"), new FFItemBase(FFRarity.RARE, "A fusion of many magical metals, probably a good vessel for enchantments.", true, true).supplier());
        sunsteel_ingot = items.register(createRL("sunsteel_ingot"), new FFItemBase(FFRarity.RARE, "The power of the sun, crystallized into a metallic form.", true, true).supplier());
        moonsilver_ingot = items.register(createRL("moonsilver_ingot"), new FFItemBase(FFRarity.RARE, "The magic of the moon seems to emanate from this metal.", true, true).supplier());
        aetherium_ingot = items.register(createRL("aetherium_ingot"), new FFItemBase(FFRarity.RARE, "Startlingly light, and it seems to be a vessel for powerful magic.", true, true).supplier());
        abyssal_ingot = items.register(createRL("abyssal_ingot"), new FFItemBase(FFRarity.RARE, "You can almost hear the shadows in this metal whispering.", true, true).supplier());

        gaia_alloy_ingot = items.register(createRL("gaia_alloy_ingot"), new FFItemBase(FFRarity.EPIC, "The ultimate combination of earthly metals.", true, true).supplier());
        celestial_alloy_ingot = items.register(createRL("celestial_alloy_ingot"), new FFItemBase(FFRarity.EPIC, "The powers of the cosmos, crystallized into a forgeable material.", true, true).supplier());

        // Raw metals
        raw_lightless = items.register(createRL("raw_lightless"), new FFItemBase().supplier());

        raw_tin = items.register(createRL("raw_tin"), new FFItemBase().supplier());
        raw_lead = items.register(createRL("raw_lead"), new FFItemBase().supplier());
        raw_aluminum = items.register(createRL("raw_aluminum"), new FFItemBase().supplier());
        raw_silver = items.register(createRL("raw_silver"), new FFItemBase().supplier());
        raw_tungsten = items.register(createRL("raw_tungsten"), new FFItemBase().supplier());

        raw_titanium = items.register(createRL("raw_titanium"), new FFItemBase(FFRarity.UNCOMMON).supplier());
        raw_platinum = items.register(createRL("raw_platinum"), new FFItemBase(FFRarity.UNCOMMON).supplier());
        raw_cobalt = items.register(createRL("raw_cobalt"), new FFItemBase(FFRarity.UNCOMMON).supplier());
        raw_osmium = items.register(createRL("raw_osmium"), new FFItemBase(FFRarity.UNCOMMON).supplier());
        raw_cryosilver = items.register(createRL("raw_cryosilver"), new FFItemBase(FFRarity.UNCOMMON).supplier());
        raw_prismarium = items.register(createRL("raw_prismarium"), new FFItemBase(FFRarity.UNCOMMON).supplier());

        raw_mithril = items.register(createRL("raw_mithril"), new FFItemBase(FFRarity.MAGIC).supplier());
        raw_orichalcum = items.register(createRL("raw_orichalcum"), new FFItemBase(FFRarity.MAGIC).supplier());
        raw_adamantite = items.register(createRL("raw_adamantite"), new FFItemBase(FFRarity.MAGIC).supplier());
        raw_khlorocyte = items.register(createRL("raw_khlorocyte"), new FFItemBase(FFRarity.MAGIC).supplier());
        raw_mycelite = items.register(createRL("raw_mycelite"), new FFItemBase(FFRarity.MAGIC).supplier());
        raw_stellarium = items.register(createRL("raw_stellarium"), new FFItemBase(FFRarity.MAGIC, "", true).supplier());
        raw_lunarium = items.register(createRL("raw_lunarium"), new FFItemBase(FFRarity.MAGIC, "", true).supplier());

    }
}
