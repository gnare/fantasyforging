/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.voxelbuster.fantasyforging.item;

import com.voxelbuster.fantasyforging.FantasyForging;
import com.voxelbuster.fantasyforging.tags.FFRarity;
import lombok.Getter;
import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.function.Supplier;

import static com.voxelbuster.fantasyforging.FantasyForging.MOD_ID;

@Getter
public class FFItemBase extends Item {

    private static final Properties defaultProperties = new Item.Properties()
        .stacksTo(64)
        .arch$tab(FantasyForging.getCreativeTabs().get(new ResourceLocation(MOD_ID, "materials_tab")));

    private final FFRarity rarity;
    private final boolean magical;
    private final Component desc;

    public FFItemBase() {
        this(FFRarity.COMMON);
    }

    public FFItemBase(FFRarity rarity) {
        this(rarity, Component.literal(""));
    }

    public FFItemBase(FFRarity rarity, String desc) {
        this(rarity, Component.literal(desc).withStyle(ChatFormatting.ITALIC), false);
    }

    public FFItemBase(FFRarity rarity, Component desc) {
        this(rarity, desc, false);
    }

    public FFItemBase(FFRarity rarity, String desc, boolean magical) {
        this(rarity, Component.literal(desc).withStyle(ChatFormatting.ITALIC), magical);
    }

    public FFItemBase(FFRarity rarity, Component desc, boolean magical) {
        this(rarity, desc, magical, defaultProperties);
    }

    public FFItemBase(FFRarity rarity, String desc, boolean magical, boolean fireproof) {
        this(rarity, Component.literal(desc).withStyle(ChatFormatting.ITALIC), magical, fireproof ? defaultProperties.fireResistant() : defaultProperties);
    }

    public FFItemBase(FFRarity rarity, Component desc, boolean magical, Properties properties) {
        super(properties);

        this.rarity = rarity;
        this.magical = magical;
        this.desc = desc;
    }

    @Override
    public boolean isFoil(ItemStack itemStack) {
        return magical || itemStack.isEnchanted();
    }

    @Override
    public Component getName(ItemStack itemStack) {
        return rarity.getFormattedComponent(super.getName(itemStack).getString());
    }

    @Override
    public void appendHoverText(ItemStack stack, @Nullable Level world, List<Component> tooltip, TooltipFlag context) {
        tooltip.add(desc);
        super.appendHoverText(stack, world, tooltip, context);
    }

    public Supplier<FFItemBase> supplier() {
        return () -> this;
    }
}
