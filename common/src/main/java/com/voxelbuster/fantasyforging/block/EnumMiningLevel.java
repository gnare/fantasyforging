package com.voxelbuster.fantasyforging.block;

import lombok.Getter;
import net.minecraft.resources.ResourceLocation;

import java.util.Arrays;

import static com.voxelbuster.fantasyforging.FantasyForging.MOD_ID;

@Getter
public enum EnumMiningLevel {
    STONE_MINING_LEVEL(new ResourceLocation("minecraft:needs_stone_tool"), 1),
    IRON_MINING_LEVEL(new ResourceLocation("minecraft:needs_iron_tool"), 2),
    DIAMOND_MINING_LEVEL(new ResourceLocation("minecraft:needs_diamond_tool"), 3),
    NETHERITE_MINING_LEVEL(new ResourceLocation(MOD_ID, "needs_tool_level_4"), 4),
    MAGICAL_MINING_LEVEL(new ResourceLocation(MOD_ID, "needs_tool_level_5"), 5),
    CELESTIAL_MINING_LEVEL(new ResourceLocation(MOD_ID, "needs_tool_level_6"), 6);

    private final ResourceLocation tag;
    private final int level;

    EnumMiningLevel(ResourceLocation tag, int level) {
        this.tag = tag;
        this.level = level;
    }

    public static EnumMiningLevel fromLevel(int level) {
        return Arrays.stream(values())
            .filter(enumMiningLevel -> level == enumMiningLevel.level)
            .findFirst()
            .orElse(null);
    }
}
