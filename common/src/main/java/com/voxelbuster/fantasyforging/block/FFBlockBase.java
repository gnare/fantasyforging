/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.voxelbuster.fantasyforging.block;

import lombok.Getter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockBehaviour;

import java.util.function.Supplier;

@Getter
public class FFBlockBase extends Block {
    private static final BlockBehaviour.Properties defaultProperties = BlockBehaviour.Properties.of().requiresCorrectToolForDrops();

    private final EnumMiningLevel miningLevel;

    public FFBlockBase(EnumMiningLevel miningLevel, float destroyTime, float explosionResistance) {
        this(miningLevel, destroyTime, explosionResistance, SoundType.STONE);
    }

    public FFBlockBase(EnumMiningLevel miningLevel, float destroyTime, float explosionResistance, SoundType soundType) {
        super(defaultProperties.destroyTime(destroyTime)
            .explosionResistance(explosionResistance)
            .sound(soundType));

        this.miningLevel = miningLevel;
    }

    public static FFBlockBase oreDeepslateVariant(EnumMiningLevel miningLevel, float destroyTime, float explosionResistance) {
        return new FFBlockBase(miningLevel, destroyTime * 1.5f, explosionResistance, SoundType.STONE);
    }

    public Supplier<FFBlockBase> supplier() {
        return () -> this;
    }
}
