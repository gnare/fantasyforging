/*
 *    Copyright 2023 Galen Nare
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.voxelbuster.fantasyforging.block;

import com.voxelbuster.fantasyforging.FantasyForging;
import com.voxelbuster.mods.datagen.annotations.BlockDataGen;
import com.voxelbuster.mods.datagen.annotations.DataGenNamespace;
import com.voxelbuster.mods.datagen.annotations.ItemDataGen;
import com.voxelbuster.mods.datagen.annotations.TagWith;
import dev.architectury.registry.registries.Registrar;
import dev.architectury.registry.registries.RegistrySupplier;
import lombok.Getter;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;

import static com.voxelbuster.fantasyforging.FantasyForging.MOD_ID;
import static com.voxelbuster.fantasyforging.FantasyForging.createRL;
import static com.voxelbuster.fantasyforging.block.EnumMiningLevel.CELESTIAL_MINING_LEVEL;
import static com.voxelbuster.fantasyforging.block.EnumMiningLevel.DIAMOND_MINING_LEVEL;
import static com.voxelbuster.fantasyforging.block.EnumMiningLevel.IRON_MINING_LEVEL;
import static com.voxelbuster.fantasyforging.block.EnumMiningLevel.MAGICAL_MINING_LEVEL;
import static com.voxelbuster.fantasyforging.block.EnumMiningLevel.NETHERITE_MINING_LEVEL;
import static com.voxelbuster.fantasyforging.block.EnumMiningLevel.STONE_MINING_LEVEL;

@Getter
@DataGenNamespace(MOD_ID)
public final class FFBlocks {

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_stone_tool", "c:blocks/ores", "c:blocks/lightless_ores", "forge:ores/lightless", "forge:ores_in_ground/stone"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/lightless_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> lightless_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_stone_tool", "c:blocks/ores", "c:blocks/tin_ores", "forge:ores/tin", "forge:ores_in_ground/stone"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/tin_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> tin_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_diamond_tool", "c:blocks/ores", "c:blocks/lead_ores", "forge:ores/lead", "forge:ores_in_ground/stone"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/lead_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> lead_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_stone_tool", "c:blocks/ores", "c:blocks/aluminum_ores", "forge:ores/aluminum", "forge:ores_in_ground/stone"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/aluminum_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> aluminum_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_iron_tool", "c:blocks/ores", "c:blocks/silver_ores", "forge:ores/silver", "forge:ores_in_ground/stone"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/silver_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> silver_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_diamond_tool", "c:blocks/ores", "c:blocks/tungsten_ores", "forge:ores/tungsten", "forge:ores_in_ground/stone"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/tungsten_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> tungsten_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_diamond_tool", "c:blocks/ores", "c:blocks/titanium_ores", "forge:ores/titanium", "forge:ores_in_ground/stone"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/titanium_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> titanium_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_diamond_tool", "c:blocks/ores", "c:blocks/platinum_ores", "forge:ores/platinum", "forge:ores_in_ground/stone"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/platinum_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> platinum_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_iron_tool", "c:blocks/ores", "c:blocks/cobalt_ores", "forge:ores/cobalt", "forge:ores_in_ground/stone"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/cobalt_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> cobalt_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_diamond_tool", "c:blocks/ores", "c:blocks/osmium_ores", "forge:ores/osmium", "forge:ores_in_ground/stone"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/osmium_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> osmium_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_diamond_tool", "c:blocks/ores", "c:blocks/cryosilver_ores", "forge:ores/cryosilver", "forge:ores_in_ground/stone"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/cryosilver_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> cryosilver_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_diamond_tool", "c:blocks/ores", "c:blocks/prismarium_ores", "forge:ores/prismarium", "forge:ores_in_ground/stone"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/prismarium_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> prismarium_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "forge:needs_netherite_tool", "fabric:needs_tool_level_4", "c:blocks/ores", "c:blocks/mithril_ores", "forge:ores/mithril", "forge:ores_in_ground/stone"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/mithril_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> mithril_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "forge:needs_netherite_tool", "fabric:needs_tool_level_4", "c:blocks/ores", "c:blocks/orichalcum_ores", "forge:ores/orichalcum", "forge:ores_in_ground/stone"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/orichalcum_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> orichalcum_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "fabric:needs_tool_level_5", "c:blocks/ores", "c:blocks/adamantite_ores", "forge:ores/adamantite", "forge:ores_in_ground/stone"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/adamantite_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> adamantite_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "fabric:needs_tool_level_5", "c:blocks/ores", "c:blocks/khlorocyte_ores", "forge:ores/khlorocyte", "forge:ores_in_ground/stone"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/khlorocyte_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> khlorocyte_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "fabric:needs_tool_level_5", "c:blocks/ores", "c:blocks/mycelite_ores", "forge:ores/mycelite", "forge:ores_in_ground/stone"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/mycelite_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> mycelite_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "fabric:needs_tool_level_6", "c:blocks/ores", "c:blocks/stellarium_ores", "forge:ores/stellarium", "forge:ores_in_ground/stone"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/stellarium_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> stellarium_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "fabric:needs_tool_level_6", "c:blocks/ores", "c:blocks/lunarium_ores", "forge:ores/lunarium", "forge:ores_in_ground/stone"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/lunarium_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> lunarium_ore;

    // Deepslate variants
    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_stone_tool", "c:blocks/ores", "c:blocks/lightless_ores", "forge:ores/lightless", "forge:ores_in_ground/deepslate"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/lightless_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> deepslate_lightless_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_stone_tool", "c:blocks/ores", "c:blocks/tin_ores", "forge:ores/tin", "forge:ores_in_ground/deepslate"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/tin_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> deepslate_tin_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_diamond_tool", "c:blocks/ores", "c:blocks/lead_ores", "forge:ores/lead", "forge:ores_in_ground/deepslate"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/lead_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> deepslate_lead_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_stone_tool", "c:blocks/ores", "c:blocks/aluminum_ores", "forge:ores/aluminum", "forge:ores_in_ground/deepslate"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/aluminum_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> deepslate_aluminum_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_iron_tool", "c:blocks/ores", "c:blocks/silver_ores", "forge:ores/silver", "forge:ores_in_ground/deepslate"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/silver_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> deepslate_silver_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_diamond_tool", "c:blocks/ores", "c:blocks/tungsten_ores", "forge:ores/tungsten", "forge:ores_in_ground/deepslate"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/tungsten_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> deepslate_tungsten_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_diamond_tool", "c:blocks/ores", "c:blocks/titanium_ores", "forge:ores/titanium", "forge:ores_in_ground/deepslate"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/titanium_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> deepslate_titanium_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_diamond_tool", "c:blocks/ores", "c:blocks/platinum_ores", "forge:ores/platinum", "forge:ores_in_ground/deepslate"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/platinum_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> deepslate_platinum_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_iron_tool", "c:blocks/ores", "c:blocks/cobalt_ores", "forge:ores/cobalt", "forge:ores_in_ground/deepslate"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/cobalt_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> deepslate_cobalt_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_diamond_tool", "c:blocks/ores", "c:blocks/osmium_ores", "forge:ores/osmium", "forge:ores_in_ground/deepslate"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/osmium_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> deepslate_osmium_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_diamond_tool", "c:blocks/ores", "c:blocks/cryosilver_ores", "forge:ores/cryosilver", "forge:ores_in_ground/deepslate"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/cryosilver_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> deepslate_cryosilver_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "minecraft:needs_diamond_tool", "c:blocks/ores", "c:blocks/prismarium_ores", "forge:ores/prismarium", "forge:ores_in_ground/deepslate"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/prismarium_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> deepslate_prismarium_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "forge:needs_netherite_tool", "fabric:needs_tool_level_4", "c:blocks/ores", "c:blocks/mithril_ores", "forge:ores/mithril", "forge:ores_in_ground/deepslate"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/mithril_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> deepslate_mithril_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "forge:needs_netherite_tool", "fabric:needs_tool_level_4", "c:blocks/ores", "c:blocks/orichalcum_ores", "forge:ores/orichalcum", "forge:ores_in_ground/deepslate"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/orichalcum_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> deepslate_orichalcum_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "fabric:needs_tool_level_5", "c:blocks/ores", "c:blocks/adamantite_ores", "forge:ores/adamantite", "forge:ores_in_ground/deepslate"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/adamantite_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> deepslate_adamantite_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "fabric:needs_tool_level_5", "c:blocks/ores", "c:blocks/khlorocyte_ores", "forge:ores/khlorocyte", "forge:ores_in_ground/deepslate"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/khlorocyte_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> deepslate_khlorocyte_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "fabric:needs_tool_level_5", "c:blocks/ores", "c:blocks/mycelite_ores", "forge:ores/mycelite", "forge:ores_in_ground/deepslate"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/mycelite_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> deepslate_mycelite_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "fabric:needs_tool_level_6", "c:blocks/ores", "c:blocks/stellarium_ores", "forge:ores/stellarium", "forge:ores_in_ground/deepslate"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/stellarium_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> deepslate_stellarium_ore;

    @TagWith(value = {"fantasy_forging:ores", "minecraft:mineable/pickaxe", "forge:ores", "fabric:needs_tool_level_6", "c:blocks/ores", "c:blocks/lunarium_ores", "forge:ores/lunarium", "forge:ores_in_ground/deepslate"}, tagNamespace = "")
    @ItemDataGen(useDefaultBlockItemDisplay = true, modelParent = "fantasy_forging:block/lunarium_ore", textures = {})
    @BlockDataGen
    private static RegistrySupplier<FFBlockBase> deepslate_lunarium_ore;

    public static void register() {


        lightless_ore = registerBlockAndItem(createRL("lightless_ore"), new FFBlockBase(IRON_MINING_LEVEL, 4, 6));

        tin_ore = registerBlockAndItem(createRL("tin_ore"), new FFBlockBase(STONE_MINING_LEVEL, 3, 3));
        lead_ore = registerBlockAndItem(createRL("lead_ore"), new FFBlockBase(IRON_MINING_LEVEL, 4, 6));
        aluminum_ore = registerBlockAndItem(createRL("aluminum_ore"), new FFBlockBase(STONE_MINING_LEVEL, 3, 3));
        silver_ore = registerBlockAndItem(createRL("silver_ore"), new FFBlockBase(IRON_MINING_LEVEL, 4, 6));

        tungsten_ore = registerBlockAndItem(createRL("tungsten_ore"), new FFBlockBase(DIAMOND_MINING_LEVEL, 5, 15));
        titanium_ore = registerBlockAndItem(createRL("titanium_ore"), new FFBlockBase(DIAMOND_MINING_LEVEL, 5, 15));
        platinum_ore = registerBlockAndItem(createRL("platinum_ore"), new FFBlockBase(DIAMOND_MINING_LEVEL, 5, 15));
        cobalt_ore = registerBlockAndItem(createRL("cobalt_ore"), new FFBlockBase(DIAMOND_MINING_LEVEL, 5, 15));
        osmium_ore = registerBlockAndItem(createRL("osmium_ore"), new FFBlockBase(NETHERITE_MINING_LEVEL, 5, 15));

        cryosilver_ore = registerBlockAndItem(createRL("cryosilver_ore"), new FFBlockBase(DIAMOND_MINING_LEVEL, 5, 15));
        prismarium_ore = registerBlockAndItem(createRL("prismarium_ore"), new FFBlockBase(DIAMOND_MINING_LEVEL, 5, 15));
        mithril_ore = registerBlockAndItem(createRL("mithril_ore"), new FFBlockBase(NETHERITE_MINING_LEVEL, 6, 30));
        orichalcum_ore = registerBlockAndItem(createRL("orichalcum_ore"), new FFBlockBase(NETHERITE_MINING_LEVEL, 6, 30));
        adamantite_ore = registerBlockAndItem(createRL("adamantite_ore"), new FFBlockBase(MAGICAL_MINING_LEVEL, 6, 30));
        khlorocyte_ore = registerBlockAndItem(createRL("khlorocyte_ore"), new FFBlockBase(MAGICAL_MINING_LEVEL, 6, 30));
        mycelite_ore = registerBlockAndItem(createRL("mycelite_ore"), new FFBlockBase(MAGICAL_MINING_LEVEL, 6, 30));

        stellarium_ore = registerBlockAndItem(createRL("stellarium_ore"), new FFBlockBase(CELESTIAL_MINING_LEVEL, 6, 100));
        lunarium_ore = registerBlockAndItem(createRL("lunarium_ore"), new FFBlockBase(CELESTIAL_MINING_LEVEL, 6, 100));

        // Deepslate variants
        deepslate_lightless_ore = registerBlockAndItem(createRL("deepslate_lightless_ore"), FFBlockBase.oreDeepslateVariant(IRON_MINING_LEVEL, 4, 6));
        deepslate_tin_ore = registerBlockAndItem(createRL("deepslate_tin_ore"), FFBlockBase.oreDeepslateVariant(STONE_MINING_LEVEL, 3, 3));
        deepslate_lead_ore = registerBlockAndItem(createRL("deepslate_lead_ore"), FFBlockBase.oreDeepslateVariant(IRON_MINING_LEVEL, 4, 6));
        deepslate_aluminum_ore = registerBlockAndItem(createRL("deepslate_aluminum_ore"), FFBlockBase.oreDeepslateVariant(STONE_MINING_LEVEL, 3, 3));
        deepslate_silver_ore = registerBlockAndItem(createRL("deepslate_silver_ore"), FFBlockBase.oreDeepslateVariant(IRON_MINING_LEVEL, 4, 6));
        deepslate_tungsten_ore = registerBlockAndItem(createRL("deepslate_tungsten_ore"), FFBlockBase.oreDeepslateVariant(DIAMOND_MINING_LEVEL, 5, 15));
        deepslate_titanium_ore = registerBlockAndItem(createRL("deepslate_titanium_ore"), FFBlockBase.oreDeepslateVariant(DIAMOND_MINING_LEVEL, 5, 15));
        deepslate_platinum_ore = registerBlockAndItem(createRL("deepslate_platinum_ore"), FFBlockBase.oreDeepslateVariant(DIAMOND_MINING_LEVEL, 5, 15));
        deepslate_cobalt_ore = registerBlockAndItem(createRL("deepslate_cobalt_ore"), FFBlockBase.oreDeepslateVariant(DIAMOND_MINING_LEVEL, 5, 15));
        deepslate_osmium_ore = registerBlockAndItem(createRL("deepslate_osmium_ore"), FFBlockBase.oreDeepslateVariant(NETHERITE_MINING_LEVEL, 5, 15));
        deepslate_cryosilver_ore = registerBlockAndItem(createRL("deepslate_cryosilver_ore"), FFBlockBase.oreDeepslateVariant(DIAMOND_MINING_LEVEL, 5, 15));
        deepslate_prismarium_ore = registerBlockAndItem(createRL("deepslate_prismarium_ore"), FFBlockBase.oreDeepslateVariant(DIAMOND_MINING_LEVEL, 5, 15));
        deepslate_mithril_ore = registerBlockAndItem(createRL("deepslate_mithril_ore"), FFBlockBase.oreDeepslateVariant(NETHERITE_MINING_LEVEL, 6, 30));
        deepslate_orichalcum_ore = registerBlockAndItem(createRL("deepslate_orichalcum_ore"), FFBlockBase.oreDeepslateVariant(NETHERITE_MINING_LEVEL, 6, 30));
        deepslate_adamantite_ore = registerBlockAndItem(createRL("deepslate_adamantite_ore"), FFBlockBase.oreDeepslateVariant(MAGICAL_MINING_LEVEL, 6, 30));
        deepslate_khlorocyte_ore = registerBlockAndItem(createRL("deepslate_khlorocyte_ore"), FFBlockBase.oreDeepslateVariant(MAGICAL_MINING_LEVEL, 6, 30));
        deepslate_mycelite_ore = registerBlockAndItem(createRL("deepslate_mycelite_ore"), FFBlockBase.oreDeepslateVariant(MAGICAL_MINING_LEVEL, 6, 30));
        deepslate_stellarium_ore = registerBlockAndItem(createRL("deepslate_stellarium_ore"), FFBlockBase.oreDeepslateVariant(CELESTIAL_MINING_LEVEL, 6, 100));
        deepslate_lunarium_ore = registerBlockAndItem(createRL("deepslate_lunarium_ore"), FFBlockBase.oreDeepslateVariant(CELESTIAL_MINING_LEVEL, 6, 100));
    }

    private static RegistrySupplier<FFBlockBase> registerBlockAndItem(ResourceLocation identifier, FFBlockBase block) {
        Registrar<Block> blocks = FantasyForging.getBlocks();
        Registrar<Item> items = FantasyForging.getItems();

        RegistrySupplier<FFBlockBase> blockReg = blocks.register(identifier, block.supplier());
        items.register(identifier, () -> new BlockItem(block, new Item.Properties().arch$tab(FantasyForging.getBlocksTab())));

        return blockReg;
    }
}
